extends Node

onready var editor = load("res://Editor.gd").new()
onready var MenuButtonFile = get_node("HBoxContainer/MenuButtonFile")
onready var MenuButtonSettings = get_node("HBoxContainer/MenuButtonSettings")
onready var tabContainer = get_node("EditorContainer/TabContainer")
onready var tabs = get_node("EditorContainer/Tabs")
onready var selectedTab = -1
onready var childCount = 0;

func _ready():
	_setup()

func _setup():
	_createFileMenu()
	_createSettingsMenu()

func _createFileMenu():
	MenuButtonFile.get_popup().add_item("Open File")
	MenuButtonFile.get_popup().add_item("Open Directory")
	MenuButtonFile.get_popup().add_item("Save as File")
	MenuButtonFile.get_popup().add_item("Quit")
	MenuButtonFile.get_popup().connect("id_pressed", self, "_on_file_item_pressed");
		
func _createSettingsMenu():
	var submenu = PopupMenu.new()

	submenu.set_name("submenuLanguage")
	submenu.add_item("GDScript")
	submenu.add_item("Python")

	MenuButtonSettings.get_popup().add_item("Layout")
	MenuButtonSettings.get_popup().add_item("Toggle Fullscreen")
	MenuButtonSettings.get_popup().add_child(submenu)
	MenuButtonSettings.get_popup().add_submenu_item("Language", "submenuLanguage")
	

func _on_file_item_pressed(id):
	match id:
	 0:
	  $FileDialog.mode = FileDialog.MODE_OPEN_FILE
	  $FileDialog.popup()
	 1:
	  print("Save File")
	 2:
	  pass
	 3:
	  get_tree().quit()

func _open_directory_selected(path):
	pass

func _on_FileDialog_file_selected(path):
	var editorIdx = -1;
	var editor = load("Editor.tscn")
	editor = editor.instance()
	var file = File.new()
	file.open(path, File.READ)
	$EditorContainer.add_child(editor)
	var editorControl = $EditorContainer.get_child(childCount)
	var windowDialog = $EditorContainer.get_child(childCount).get_node("WindowDialog")
	var textEdit = windowDialog.get_node("TextEdit")
	windowDialog.window_title = path
	textEdit.text = file.get_as_text()
	editorControl.rect_position = Vector2(100, 100)
	editorControl.rect_size = Vector2(800, 600)
	print(childCount)
	childCount = childCount + 1
	
func _on_TabContainer_tab_selected(tab):
	selectedTab = tab


func _on_Tabs_tab_clicked(tab):
	tabContainer.current_tab = tab

